package com.minimalart.salarycalculator;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;

import java.util.Locale;

/**
 * Created by ytgab on 7/11/2017.
 */

public class ApplicationMain extends Application {

    private static final String KEY_LANGUAGE = "lang_key";
    private static final String LANG_RO = "ro";
    private static final String LANG_EN = "en";

    private Locale locale = null;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            Locale.setDefault(locale);
            Configuration config = new Configuration(newConfig);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        String lang = null;
        if(!settings.contains(KEY_LANGUAGE))
            settings.edit().putString(KEY_LANGUAGE, LANG_RO).commit();
        lang = settings.getString(KEY_LANGUAGE, LANG_RO);
        changeLang(lang);
    }

    public void changeLang(String lang) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        if (!"".equals(lang) && !config.locale.getLanguage().equals(lang)) {

            SharedPreferences.Editor ed = PreferenceManager.getDefaultSharedPreferences(this).edit();
            ed.putString(KEY_LANGUAGE, lang);
            ed.commit();

            locale = new Locale(lang);
            Locale.setDefault(locale);
            Configuration conf = new Configuration(config);
            conf.locale = locale;
            getBaseContext().getResources().updateConfiguration(conf, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    public String getLang(){
        return PreferenceManager.getDefaultSharedPreferences(this).getString(KEY_LANGUAGE, LANG_RO);
    }
}
