package com.minimalart.salarycalculator;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telecom.Call;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import java.text.DecimalFormat;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        CompoundButton.OnCheckedChangeListener,
        TextWatcher,
        TextView.OnEditorActionListener, View.OnFocusChangeListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    /**
     * Taxe angajat:
     *
     * Contribuție Fondul de Șomaj - 0.5%
     * Contribuție Asigurări Sociale - 10.5%
     * Contribuție Asigurări Sociale de Sănătate - 5.5%
     * Impozit pe venit - 16%
     */
    private static final double PERCENT_CFS = 0.005;
    private static final double PERCENT_CAS = 0.105;
    private static final double PERCENT_CASS = 0.055;
    private static final double PERCENT_TAX = 0.16;

    /**
     * Taxe angajator:
     *
     * Contribuție Asigurări Sociale - 15.8%
     * Contribuție Fondul de Șomaj - 0.5%
     * Contribuție Fondul de Concedii și Indemnizații - 0.85%
     * Contribuție Fondul de Garantare al Plății Creanțelor Salariale - 0.25%
     * Contribuție Asigurări Sociale de Sănătate - 5.2%
     * Contribuție Fondul de Accidente de Muncă și Boli Profesionale - 0.4%
     */
    private static final double PERCENT_EMP_CAS = 0.158;
    private static final double PERCENT_EMP_CASS = 0.052;
    private static final double PERCENT_EMP_CFS = 0.005;
    private static final double PERCENT_EMP_CFCI = 0.0085;
    private static final double PERCENT_EMP_CFGPCS = 0.0025;
    private static final double PERCENT_EMP_CFAMBP = 0.004;

    private static final double PERCENT_EMP_TOTAL_TAXES =
            PERCENT_EMP_CAS +
            PERCENT_EMP_CASS +
            PERCENT_EMP_CFS +
            PERCENT_EMP_CFCI +
            PERCENT_EMP_CFGPCS +
            PERCENT_EMP_CFAMBP;

    private TextInputEditText brutSalary;
    private FloatingActionButton fab;

    private View calculatedGroup;

    private TextView employeeCAS;
    private TextView employeeCASS;
    private TextView employeeCFS;
    private TextView employeeDP;
    private TextView employeeTAX;
    private TextView employeeTAXTotal;

    private TextView employerCAS;
    private TextView employerCASS;
    private TextView employerCFS;
    private TextView employerCFCI;
    private TextView employerCFGPCS;
    private TextView employerCFAMBP;
    private TextView employerTAXTotal;

    private TextView employeeNetSalary;
    private TextView employerTotalCost;

    private Spinner personsList;
    private CheckBox programmer;

    private static final String KEY_LANGUAGE = "lang_key";
    private static final String LANG_RO = "ro";
    private static final String LANG_EN = "en";

    private SharedPreferences prefs;

    private static String CURRENCY;
    private static String CURRENCY_RON = " lei";
    private static String CURRENCY_EUR = " euro";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            getWindow().setNavigationBarColor(ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryDark));

        initViews();

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        setLang();
        fab.setOnClickListener(this);
    }

    private void setLang() {

    }

    private void initViews() {
        fab = (FloatingActionButton) findViewById(R.id.fab);
        brutSalary = (TextInputEditText) findViewById(R.id.edit_brut);
        brutSalary.addTextChangedListener(this);
        brutSalary.setOnEditorActionListener(this);
        brutSalary.setOnFocusChangeListener(this);

        personsList = (Spinner) findViewById(R.id.dropdown_persons);
        programmer = (CheckBox) findViewById(R.id.checkbox_programmer);
        programmer.setOnCheckedChangeListener(this);

        employeeCAS = (TextView) findViewById(R.id.textview_cas);
        employeeCASS = (TextView) findViewById(R.id.textview_cass);
        employeeCFS = (TextView) findViewById(R.id.textview_cfs);
        employeeDP = (TextView) findViewById(R.id.textview_dp);
        employeeTAX = (TextView) findViewById(R.id.textview_tax);
        employeeTAXTotal = (TextView) findViewById(R.id.textview_total_employee);

        employerCAS = (TextView) findViewById(R.id.textview_cas_employer);
        employerCASS = (TextView) findViewById(R.id.textview_cass_employer);
        employerCFS = (TextView) findViewById(R.id.textview_cfs_employer);
        employerCFCI = (TextView) findViewById(R.id.textview_cfci);
        employerCFGPCS = (TextView) findViewById(R.id.textview_cfgps);
        employerCFAMBP = (TextView) findViewById(R.id.textview_cfambp);
        employerTAXTotal = (TextView) findViewById(R.id.textview_total_employer);

        employeeNetSalary = (TextView) findViewById(R.id.salary_net);
        employerTotalCost = (TextView) findViewById(R.id.total_paid_by_employer);

        calculatedGroup = findViewById(R.id.group_calculated_items);
        calculatedGroup.setVisibility(View.GONE);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_change_language){
            String lan = prefs.getString(KEY_LANGUAGE, LANG_RO);
            if(lan.equals(LANG_RO))
                ((ApplicationMain)getApplication()).changeLang(LANG_EN);
            else if(lan.equals(LANG_EN))
                ((ApplicationMain)getApplication()).changeLang(LANG_RO);

            restartApp();
        }

        return super.onOptionsItemSelected(item);
    }

    private void restartApp() {
        Intent restartActivity = new Intent(MainActivity.this, MainActivity.class);
        int pendingIntentId = 111;

        PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, pendingIntentId, restartActivity,
                PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) MainActivity.this.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis(), pendingIntent);
        System.exit(0);
    }

    @Override
    public void finish() {
        super.finish();
        Log.v(TAG, "here");
    }

    @Override
    public void onClick(View view) {
        closeKeyboard(1);
        testSalary();
    }

    private void testSalary() {
        if(!brutSalary.getText().toString().isEmpty()) {
            if (!brutSalary.getText().toString().substring(0, 1).equals("0")) {
                calculateTaxes(Double.parseDouble(brutSalary.getText().toString()));
            } else
                brutSalary.setError(getResources().getString(R.string.incorrect_value));
        }else{
            calculateTaxes(0);
            brutSalary.setError(getResources().getString(R.string.error_empty));

        }
    }

    private void closeKeyboard(int id) {
        View view = this.getCurrentFocus();

        if(id == 1) {
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void calculateTaxes(double brutSalaryValue) {
        int persons;
        int personPoz = personsList.getSelectedItemPosition();
        persons = Integer.valueOf(getResources().getStringArray(R.array.entries_prompt)[personPoz]);

        CURRENCY = CURRENCY_RON;

        double empCAS = brutSalaryValue * PERCENT_CAS;
        double empCASS = brutSalaryValue * PERCENT_CASS;
        double empCFS = brutSalaryValue * PERCENT_CFS;
        double empDP = 300 + 100 * persons;

        if(brutSalaryValue > 1500){
            empDP = empDP * (1D - ((brutSalaryValue - 1500D) / 1500D));
        }

        empDP = 0D;

        double empVCI = brutSalaryValue - (empCFS + empCAS + empCASS + empDP);
        double empTAX;

        if(!programmer.isChecked())
            empTAX = empVCI * PERCENT_TAX;
        else
            empTAX = 0;

        double empTotalTax = round(empCAS) + round(empCASS) + round(empCFS) + round(empTAX);

        employeeCAS.setText(convertDoubleToText(empCAS) + CURRENCY);
        employeeCASS.setText(convertDoubleToText(empCASS) + CURRENCY);
        employeeCFS.setText(convertDoubleToText(empCFS) + CURRENCY);
        employeeDP.setText(convertDoubleToText(empDP) + CURRENCY);
        employeeTAX.setText(convertDoubleToText(empTAX) + CURRENCY);
        employeeTAXTotal.setText(convertDoubleToText(empTotalTax) + CURRENCY);

        double emprCAS = round(brutSalaryValue * PERCENT_EMP_CAS);
        double emprCASS = round(brutSalaryValue * PERCENT_EMP_CASS);
        double emprCFS = round(brutSalaryValue * PERCENT_EMP_CFS);
        double emprCFCI = round(brutSalaryValue * PERCENT_EMP_CFCI);
        double emprCFGPCS = round(brutSalaryValue * PERCENT_EMP_CFGPCS);
        double emprCFAMBP = round(brutSalaryValue * PERCENT_EMP_CFAMBP);
        double emprTAXTotal = emprCAS +
                emprCASS +
                emprCFS +
                emprCFCI +
                emprCFGPCS +
                emprCFAMBP;

        employerCAS.setText(convertDoubleToText(emprCAS) + CURRENCY);
        employerCASS.setText(convertDoubleToText(emprCASS) + CURRENCY);
        employerCFS.setText(convertDoubleToText(emprCFS) + CURRENCY);
        employerCFCI.setText(convertDoubleToText(emprCFCI) + CURRENCY);
        employerCFGPCS.setText(convertDoubleToText(emprCFGPCS) + CURRENCY);
        employerCFAMBP.setText(convertDoubleToText(emprCFAMBP) + CURRENCY);
        employerTAXTotal.setText(convertDoubleToText(emprTAXTotal) + CURRENCY);

        double empNetSalary = round(brutSalaryValue - empTotalTax);
        employeeNetSalary.setText(convertDoubleToText(empNetSalary) + CURRENCY);

        double emprTotalCost = brutSalaryValue + (
                emprCAS +
                emprCASS +
                emprCFS +
                emprCFCI +
                emprCFGPCS +
                emprCFAMBP);
        employerTotalCost.setText(convertDoubleToText(emprTotalCost) + CURRENCY);

        if(calculatedGroup.getVisibility() == View.GONE){
            calculatedGroup.setVisibility(View.VISIBLE);
            calculatedGroup.setAlpha(0.0f);
            calculatedGroup.animate()
                    .translationY(calculatedGroup.getHeight())
                    .alpha(1.0f);
        }

        Log.v(TAG, "end");
    }

    public String convertDoubleToText(double value){
        Log.v(TAG, "before: " + String.valueOf(value));
        value = round(value);
        Log.v(TAG, "before: " + String.valueOf(value));
        Log.v(TAG, "  ");

        DecimalFormat decFormat = new DecimalFormat();
        decFormat.setDecimalSeparatorAlwaysShown(false);

        return decFormat.format(value);
    }

    public double round(double value) {
        return (double)Math.round(value);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if(calculatedGroup.getVisibility() == View.VISIBLE) {
            testSalary();
            closeKeyboard(1);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if(calculatedGroup.getVisibility() == View.VISIBLE)
            testSalary();
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if(i == EditorInfo.IME_ACTION_DONE){
            testSalary();
            closeKeyboard(1);
            return true;
        }

        return false;
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if(!b)
            closeKeyboard(1);
    }
}
